# example-java-8
Proyecto en Java 8 que levanta un recurso GET
 ~~~~
 localhost:8080/user
~~~~

se compila mediante:
~~~~
mvn clean install
~~~~

## Pasos para migrar de Java 8 a Java 11
- Modificar java.version a 11
- Quitar plugin spring-boot-maven
- Añadir maven-compiler-plugin 3.7.0
~~~~
    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.7.0</version>
        <configuration>
            <release>${java.version}</release>
        </configuration>
    </plugin>
~~~~
- Compilar y arrancar

## Crear ModuleSystem
- Crear module-info.java en la raíz
- Compilar y ver que falla
- Ir añadiendo modulos hasta que compile
- Eliminar lombok debido a un error de soporte y no encuentra los modulos ( al menos yo no lo he resuelto )
- Levantar y ver que falla porque no encuentra java.sql -> Añadir al module-info
- Falla javax.annotation
~~~~
    <dependency>
        <groupId>javax.annotation</groupId>
        <artifactId>javax.annotation-api</artifactId>
    </dependency>
~~~~



