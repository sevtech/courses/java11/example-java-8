package com.sevtech.examplejava8.service;

import com.sevtech.examplejava8.model.User;
import org.springframework.stereotype.Service;

@Service
public class UserGeneratorService {

    public User generateUser() {
        return User.builder().age(26).name("Jesus").surname("Jimenez").salary(70000).build();
    }
}
