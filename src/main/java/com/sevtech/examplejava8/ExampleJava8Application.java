package com.sevtech.examplejava8;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExampleJava8Application {

	public static void main(String[] args) {
		SpringApplication.run(ExampleJava8Application.class, args);
	}

}
